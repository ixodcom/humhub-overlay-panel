<?php

class m140930_046123_initial extends EDbMigration
{

    public function up()
    {
        $this->createTable('overlay_panel', array(
            'id' => 'pk',
            'type' => 'smallint(6) NOT NULL',
            'title' => 'varchar(255) NOT NULL',
            'icon' => 'varchar(100)',
            'content' => 'TEXT',
            'sort_order' => 'int(11)',
            'navigation_class' => 'varchar(255) NOT NULL',
        	'use_as'=>'int(11)',
        	'slider'=>'int(11)',
        	'slider_position'=>'varchar(100) NOT NULL',
                ), '');
    }

    public function down()
    {
        echo "m140930_046123_initial does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
