<?php

class uninstall extends ZDbMigration
{

    public function up()
    {

        $this->dropTable('overlay_panel');
    }

    public function down()
    {
        echo "uninstall does not support migration down.\n";
        return false;
    }

}
