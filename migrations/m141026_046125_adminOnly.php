<?php

class m141026_046125_adminOnly extends EDbMigration
{

    public function up()
    {
        $this->addColumn('overlay_panel', 'admin_only', 'boolean DEFAULT 0');
    }

    public function down()
    {
        echo "m141026_046125_adminOnly does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
