<?php

/**
 * HumHub
 * Copyright © 2014 The HumHub Project
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 */

/**
 * Description of OverlayPanelEvents
 *
 * @author luke
 */
class OverlayPanelEvents
{

	
	
	public static function init(){
	
		Yii::app()->clientScript->registerScript("search", "$(window).load(function(){
				$.ajax({
				url:'".Yii::app()->createUrl('//overlay_panel/view/popup')."',
				success:function(response){
				$('body').append(response);
				}})
				});"
		);
	}
	
    public static function onAdminMenuInit($event)
    {
    	
    	self::init();
        $event->sender->addItem(array(
            'label' => Yii::t('OverlayPanelModule.base', 'Overlay Panel'),
            'url' => Yii::app()->createUrl('//overlay_panel/admin'),
            'group' => 'manage',
            'icon' => '<i class="fa fa-file-o"></i>',
            'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'overlay_panel' && Yii::app()->controller->id == 'admin'),
            'sortOrder' => 300,
        ));

        // Check for Admin Menu Pages to insert
    }

    public static function onTopMenuInit($event)
    {
    	self::init();
        foreach (OverlayPanel::model()->findAllByAttributes(array('navigation_class' => OverlayPanel::NAV_CLASS_TOPNAV)) as $page) {
        	 
        	

            // Admin only
            if ($page->admin_only == 1 && !Yii::app()->user->isAdmin()) {
                continue;
            }

            $event->sender->addItem(array(
                'label' => $page->title,
            	
            		
            		'htmlOptions' => array(
            				'id'=>'slidenav',
            				'class'=>'op-tab',
			            	'data-pos'=>$page->slider_position,
			            	'data-panelid'=>'panel1',
			            	'data-pageid'=>$page->id,
            		),
            	
                'url' => 'javascript:void(0)',
                'target' => ($page->type == OverlayPanel::TYPE_LINK) ? '_blank' : '',
                'icon' => '<i class="fa '.trim($page->icon).'"></i>',
                'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'overlay_panel' && Yii::app()->controller->id == 'view' && Yii::app()->request->getParam('id') == $page->id),
                'sortOrder' => ($page->sort_order != '') ? $page->sort_order : 1000,
            ));
        }
    }

    public static function onAccountMenuInit($event)
    {
    	self::init();
        foreach (OverlayPanel::model()->findAllByAttributes(array('navigation_class' => OverlayPanel::NAV_CLASS_ACCOUNTNAV)) as $page) {
            // Admin only
            if ($page->admin_only == 1 && !Yii::app()->user->isAdmin()) {
                continue;
            }

            $event->sender->addItem(array(
                'label' => $page->title,
            	'htmlOptions' => array(
            			'id'=>'slidenav',
            				'class'=>'op-tab',
			            	'data-pos'=>$page->slider_position,
			            	'data-panelid'=>'panel1',
			            	'data-pageid'=>$page->id,
            		),
                'url' =>'javascript:void(0)',
            	
                'target' => ($page->type == OverlayPanel::TYPE_LINK) ? '_blank' : '',
                'icon' => '<i class="fa '.trim($page->icon).'"></i>',
                'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'overlay_panel' && Yii::app()->controller->id == 'view' && Yii::app()->request->getParam('id') == $page->id),
                'sortOrder' => ($page->sort_order != '') ? $page->sort_order : 1000,
            ));
        }
    }

}
