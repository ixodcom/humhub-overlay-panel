<?php

/**
 * This is the model class for table "overlay_panel_page".
 *
 * The followings are the available columns in table 'overlay_panel_page':
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property string $icon
 * @property string $content
 * @property integer $sort_order
 * @property integer $admin_only
 * @property string $navigation_class
 */
class OverlayPanel extends HActiveRecord
{

    public $url;

    const NAV_CLASS_TOPNAV = 'TopMenuWidget';
    const NAV_CLASS_ACCOUNTNAV = 'AccountMenuWidget';
    const TYPE_LINK = '1';
    const TYPE_HTML = '2';
    const TYPE_IFRAME = '3';
    const TYPE_MARKDOWN = '4';
    
    const TYPE_LEFT = 'left';
    const TYPE_RIGHT = 'right';
    const TYPE_BUTTON = 'bottom';
    const TYPE_TOP = 'top';
    
    const TYPE_PAGE = '0';
    const TYPE_SLIDER = '1';
    
    const TYPE_SLIDEINPANEL = '0';
    const TYPE_OPENPANEL = '1';
   

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OverlayPanel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'overlay_panel';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, title, navigation_class', 'required'),
            array('type, sort_order, admin_only', 'numerical', 'integerOnly' => true),
            array('title, navigation_class', 'length', 'max' => 255),
            array('icon', 'length', 'max' => 100),
            array('content, url, slider_position, use_as, slider', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'icon' => 'Icon',
            'content' => 'Content',
            'url' => 'URL',
            'sort_order' => 'Sort Order',
            'admin_only' => 'Only visible for admins',
            'navigation_class' => 'Navigation',
        );
    }

    public function beforeSave()
    {
        if ($this->type == self::TYPE_IFRAME || $this->type == self::TYPE_LINK) {
            $this->content = $this->url;
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        if ($this->type == self::TYPE_IFRAME || $this->type == self::TYPE_LINK) {
            $this->url = $this->content;
        }

        return parent::afterFind();
    }

    public static function getNavigationClasses()
    {
        return array(
            self::NAV_CLASS_TOPNAV => Yii::t('OverlayPanelModule.base', 'Top Navigation'),
            self::NAV_CLASS_ACCOUNTNAV => Yii::t('OverlayPanelModule.base', 'User Account Menu (Settings)'),
        );
    }

    public static function getPageTypes()
    {
        return array(
            //self::TYPE_LINK => Yii::t('OverlayPanelModule.base', 'Link'),
            self::TYPE_HTML => Yii::t('OverlayPanelModule.base', 'HTML'),
            self::TYPE_MARKDOWN => Yii::t('OverlayPanelModule.base', 'MarkDown'),
            self::TYPE_IFRAME => Yii::t('OverlayPanelModule.base', 'IFrame'),
        );
    }
    
    
    
    public static function getSliderPosition()
    {
    	return array(
    			self::TYPE_LEFT => Yii::t('OverlayPanelModule.base', 'Left'),
    			self::TYPE_RIGHT => Yii::t('OverlayPanelModule.base', 'Right'),
    			self::TYPE_BUTTON => Yii::t('OverlayPanelModule.base', 'Botton'),
    			self::TYPE_TOP => Yii::t('OverlayPanelModule.base', 'Top'),
    	);
    }
    
    
    public static function getUseAs()
    {
    	return array(
    			self::TYPE_PAGE => Yii::t('OverlayPanelModule.base', 'Page'),
    			self::TYPE_SLIDER => Yii::t('OverlayPanelModule.base', 'Slider'),
    			
    	);
    }
    
    
     public static function getSlider(){ 
    
    	
    	return array(
    			//self::TYPE_SLIDEINPANEL => Yii::t('OverlayPanelModule.base', 'Slide in Panel'),
    			self::TYPE_OPENPANEL => Yii::t('OverlayPanelModule.base', 'Open Panel'),
    	
    	);
    }

}
