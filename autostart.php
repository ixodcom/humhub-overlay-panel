<?php Yii::app()->moduleManager->register(array(
    'id' => 'overlay_panel',
    'class' => 'application.modules.overlay_panel.OverlayPanelModule',
    'import' => array(
        'application.modules.overlay_panel.*',
        'application.modules.overlay_panel.models.*',
    ),
    // Events to Catch 
    'events' => array(
        array('class' => 'AdminMenuWidget', 'event' => 'onInit', 'callback' => array('OverlayPanelEvents', 'onAdminMenuInit')),
        array('class' => 'TopMenuWidget', 'event' => 'onInit', 'callback' => array('OverlayPanelEvents', 'onTopMenuInit')),
        array('class' => 'AccountMenuWidget', 'event' => 'onInit', 'callback' => array('OverlayPanelEvents', 'onAccountMenuInit')),
    ),
));
?>