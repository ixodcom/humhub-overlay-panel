

<?php if ($navigationClass == OverlayPanel::NAV_CLASS_ACCOUNTNAV): ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <?php
            $parser = new CMarkdownParser;
            echo $parser->safeTransform($md);
            ?>

        </div>
    </div>

<?php else: ?>

    <div class="container panelcontainer">

        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php
                    $parser = new CMarkdownParser;
                    echo $parser->safeTransform($md);
                    ?>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>