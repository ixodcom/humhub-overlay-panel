<?php
return array (
  'Create new Page' => '',
  'Custom Pages' => 'Páginas personalizadas',
  'HTML' => 'HTML',
  'IFrame' => 'IFrame',
  'Link' => 'Link',
  'MarkDown' => 'MarkDown',
  'Navigation' => 'Navegación',
  'No custom pages created yet!' => '¡No se han creado páginas personalizadas todavía!',
  'Sort Order' => '',
  'Title' => 'Título',
  'Top Navigation' => '',
  'Type' => 'Tipo',
  'User Account Menu (Settings)' => '',
);
