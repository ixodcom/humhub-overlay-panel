<?php

class OverlayPanelModule extends HWebModule
{

	private $_assetsUrl;
    public $subLayout = "application.modules_core.admin.views._layout";

    
    public function getConfigUrl()
    {
        return Yii::app()->createUrl('//overlay_panel/admin');
    }

    public function disable()
    {
        if (parent::disable()) {

            foreach (OverlayPanel::model()->findAll() as $entry) {
                $entry->delete();
            }

            return true;
        }

        return false;
    }
    
    
    
    public function getAssetsUrl()
    {
    	if ($this->_assetsUrl === null)
    		$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('overlay_panel.assets') );
    	
    
    	return $this->_assetsUrl;
    }

}
